@extends('layouts.master')
@section('content')
	<div class="card">
		<div class="card-header" style="border-top:3px solid black;">
			<h5>Add Student</h5>
		</div>
		<div class="card-body" style="border-top:3px solid black;">
			<form action="/students/store" method="POST">
				@csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label for="First Name">First Name:</label>
							<input type="text" name="first_name" placeholder="First Name" class='form-control' required>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="Middle Initial">Middle Initial:</label>
							<input type="text" name="middle_initial" placeholder="Middle Initial" class='form-control'>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="Last Name">Last Name:</label>
							<input type="text" name="last_name" placeholder="Last Name" class='form-control' required>
						</div>
					</div>
					
					<div class="col-sm-4 d-flex">
						<div class="form-group">
							<label for="Academic Program">Academic Program</label>
							<select style="width:175%;" name="academic_program_id" id="" class="form-control">
								@foreach($academic_programs as $category)
									<option value="{{ $category->id }}">{{ $category->degree_name }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="col-sm-12 d-flex">
						<div class="form-group">
							<label for="Adviser">Advised By:</label>
							<select style="width:175%;" name="adviser_id" id="" class="form-control" required="">
								<option></option>
								@foreach($advisers as $adviser)
									<option value="{{ $adviser->id }}" class="form-control">{{ $adviser->advisername  }}</option>
								@endforeach
							</select>
						</div>
					</div>



					<div class="col-sm-12 d-flex">
						<div class="form-group">
							{{-- <label for="Instructor">Added By:</label> --}}
							<select style="width:175%;" name="user_id" id="" class="form-control" hidden>
									<option value="{{  auth()->user()->id  }}" class="form-control">{{ auth()->user()->name }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="d-flex">
					<div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
					<div class="col-sm-6 d-flex justify-content-end">
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</form>			
		</div>
	</div>
@endsection
