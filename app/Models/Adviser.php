<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Adviser extends Model
{

	protected $guarded = [];

	 public function students()
    {
        return $this->hasMany(Student::class);
    }

     public function users()
    {
        return $this->hasMany(User::class);
    }


    public function getAdviserNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
  

    use HasFactory;
}
