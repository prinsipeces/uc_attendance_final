<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $guarded = [];
	
	public function students()
	{
		return $this->hasMany(Student::class);
	}

	public function getSubNameAttribute()
    {
        return $this->subject_code.' - '.$this->subject_name;
    }
    public function getSubjectScheduleAttribute()
    {
        return $this->subject_start_day.''.$this->subject_end_day.' - '.$this->subject_start_time.'-'.$this->subject_end_time;
    }

    use HasFactory;
}
