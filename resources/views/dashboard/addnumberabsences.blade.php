@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Add Absences</h5>
        </div>
        <div class="card-body" style="border:2px solid black;">
            @foreach($student->absents as $absent)  
            <form action="/dashboard/{{ $absent->id }}/updatenumberabsences" method="POST">
            @endforeach
                @csrf
                <div class="row">

                        <div class="col-sm-4 d-flex">
                            <div class="form-group">
                                <label for="Student Name">Student Name:</label>
                                <select name="student_id" id="" class="form-control">
                                    <option value="{{ $student->id }}">{{ $student->fullname }}</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="Number of Absent">Number of Absent:</label>
                                <input type="number" name="num_absent" min="0" max="20" oninput="this.value = Math.abs(this.value)" placeholder="How many?" class='form-control'>
                            </div>
                        </div>
                </div>

                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>
@endsection








