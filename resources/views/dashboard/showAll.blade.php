@extends('layouts.master')
@section('content')
	<div class="card table-responsive">
		<div class="card-header">
			<h5>Students</h5>
		</div>
		<div class="card-body">

<div class="panel-body">
    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
		
			<div class="col-sm-12 d-flex justify-content-end">
				<form action="/search" method="get">
					<div class="form-group d-flex justify-content-end">
						<input type="text" name="search" class="form-control" placeholder="Search">
						<span class="form-group-btn col-sm-3 d-flex justify-content-end">
							<button type="submit" class="btn btn-primary">Search</button>
						</span>
						<span class="form-group-btn sm-2 d-flex justify-content-end">
							<a href="/dashboard/showAll" class="btn btn-dark">Reset</a>
						</span>
					</div>
				</form>
			</div>

			<table class="table-bordered table-dark table-hover">
				<thead>
					<tr>
						<th>Name of Student</th>
						<th>Academic Program</th>
						<th>Subject</th>
						<th>Schedule</th>
						<th>Date of Absences</th>
						<th>Number of Absences</th>
						<th>Date of Tardiness</th>
						<th>Number of Tardiness</th>
						<th></th>
					</tr>
				</thead>
				<tbody style="font-size: 10px">
					@foreach($students as $student)
						<tr>
							<td>{{ $student->fullname }}</td>
							<td>{{ $student->academic_programs['degree_name'] }}</td>
						
							<td style="width: 10%">@foreach($student->subjects as $subject)
									<option value="{{ $student->subjects }}">{{ $subject->subname }}</option>
									<a href="/dashboard/{{ $subject->id }}/editsubject" class="btn btn-info" style="font-size: 10px;height: 20px; line-height: 7px;">Edit</a>
									<a href="/dashboard/{{ $subject->id }}/delesubject" class="btn btn-danger" style="font-size: 10px;height: 20px; line-height: 7px;">Delete</a><hr style="border:2px solid">
								@endforeach
							</td>

							<td>@foreach($student->subjects as $subject)
									<option value="{{ $student->subjects }}">{{ $subject->subjectschedule}}</option><br><hr style="border:2px solid">
								@endforeach
							</td>

							<td>@foreach($student->absents as $absent)
									<option>{{ $absent->date}}</option>
								@endforeach
								<a href="/dashboard/{{ $student->id }}/addabsences" class="btn btn-info" style="font-size: 10px;height: 20px; line-height: 7px;">Add</a>
							</td>
						
							<td>@foreach($student->absents as $absent)
									<option>{{ $absent->num_absent}}</option>
								@endforeach
								<a href="/dashboard/{{ $student->id }}/addnumberabsences" class="btn btn-info" style="font-size: 10px;height: 20px; line-height: 7px;">Update</a>
							</td>

							<td>
								@foreach($student->lates as $late)
									<option>{{ $late->date}}</option>
								@endforeach
								<a href="/dashboard/{{ $student->id }}/addtardiness" class="btn btn-info" style="font-size: 10px;height: 20px; line-height: 7px;">Add</a>
							</td>

							<td>@foreach($student->lates as $late)
									<option>{{ $late->number_late}}</option>
								@endforeach
								<a href="/dashboard/{{ $student->id }}/addnumbertardiness" class="btn btn-info" style="font-size: 10px;height: 20px; line-height: 7px;">Update</a>
							</td>

							<td>
								<a href="/dashboard/{{ $student->id }}/edit" class="btn btn-info" style="height: 35px;">&nbsp&nbspEdit&nbsp&nbsp</a>
								<div class="dropdown-divider"></div>
								<a href="/dashboard/{{ $student->id }}/delete" class="btn btn-danger" style="height: 35px;">Delete</a>
							</td>
							
						</tr>
					@endforeach
				</tbody>
			</table>
			{{ $students->links() }}
		</div>
	</div>
	</div>
@endsection
