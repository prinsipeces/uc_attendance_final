<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adminRole = Role::where('role_name', 'Admin')->first();
        $teacherRole = Role::where('role_name', 'Teacher')->first();



        $Admin = User::create([
            'id_role' => 1,
            'username' => 'admin',
            'password' => bcrypt('Admin1234.'),
            'first_name' => 'CITCS',
            'last_name' => 'Administrator'
        ]);

        $Teacher = User::create([
            'id_role' => 2,
            'username' => 'user',
            'password' => bcrypt('User1234.'),
            'first_name' => 'CITCS',
            'last_name' => 'Instructor'
        ]);


        $Admin->roles()->attach($adminRole);
        $Teacher->roles()->attach($teacherRole);
    }
}
