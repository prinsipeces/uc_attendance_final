
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
*/

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::post('/login', 'App\Http\Controllers\LoginController@store');

Route::middleware('auth')
    ->group(function() {
        //logout
        Route::get('/logout', 'App\Http\Controllers\LoginController@logout');

        //ADMIN PANEL
        //index
        Route::get('/admin', 'App\Http\Controllers\AdminController@index')->middleware('auth.admin');
        //show all students
        Route::get('/admin/showallstudents', 'App\Http\Controllers\AdminController@showStudents')->middleware('auth.admin');
        //create users
        Route::get('/admin/adduser', 'App\Http\Controllers\AdminController@createUser')->middleware('auth.admin');
        //store users
        Route::post('/admin/storeuser', 'App\Http\Controllers\AdminController@storeUser')->middleware('auth.admin');
        //show users
        Route::get('/admin/showuser', 'App\Http\Controllers\AdminController@showUser')->middleware('auth.admin');
        //edit user
        Route::get('/admin/{users}/edituser', 'App\Http\Controllers\AdminController@editUser')->middleware('auth.admin');
        //update user
        Route::post('/admin/{users}/updatesubject', 'App\Http\Controllers\AdminController@updateUser')->middleware('auth.admin');
        //delete user
        Route::get('/admin/{users}/deleteuser', 'App\Http\Controllers\AdminController@deleteUser')->middleware('auth.admin');
        //add adviser
        Route::get('/admin/addadviser', 'App\Http\Controllers\AdminController@addAdviser')->middleware('auth.admin');
        //store adviser
        Route::post('/admin/storeadviser', 'App\Http\Controllers\AdminController@storeAdviser')->middleware('auth.admin');
        //show advisers
        Route::get('/admin/showadviser', 'App\Http\Controllers\AdminController@showAdviser')->middleware('auth.admin');
        //edit adviser
        Route::get('/admin/{adviser}/editadviser', 'App\Http\Controllers\AdminController@editAdviser')->middleware('auth.admin');
        //update adviser
        Route::post('/admin/{adviser}/updateadviser', 'App\Http\Controllers\AdminController@updateAdviser')->middleware('auth.admin');
        //delete adviser
        Route::get('/admin/{adviser}/deleteadviser', 'App\Http\Controllers\AdminController@deleteAdviser')->middleware('auth.admin');
        //search on index/home
        Route::get('/searchindex/','App\Http\Controllers\AdminController@searchIndex')->middleware('auth.admin');
        //search students
        Route::get('/searchstudents/','App\Http\Controllers\AdminController@searchStudent')->middleware('auth.admin');
        //search on showusers
        Route::get('/usersearch/','App\Http\Controllers\AdminController@searchUser')->middleware('auth.admin');
        //search on advisers
        Route::get('/advisersearch/','App\Http\Controllers\AdminController@searchAdviser')->middleware('auth.admin');
        //change password
        Route::get('admin/changepassword','App\Http\Controllers\LoginController@showChangePasswordFormAdmin')->middleware('auth.admin');
        Route::post('admin/changePassword','App\Http\Controllers\LoginController@changePasswordAdmin')->middleware('auth.admin');
        //export to word
       // Route::get('admin/word/{id}', 'App\Http\Controllers\LoginController@wordExport');

        //TEACHER PANEL
        //index
        Route::get('/students', 'App\Http\Controllers\StudentsController@index')->middleware('auth.teacher');
        //createstudents
        Route::get('/students/createstudents', 'App\Http\Controllers\StudentsController@create')->middleware('auth.teacher');
        //storestudents
        Route::post('/students/store', 'App\Http\Controllers\StudentsController@store')->middleware('auth.teacher');
        //edit student
        Route::get('/dashboard/{student}/edit', 'App\Http\Controllers\StudentsController@edit')->middleware('auth.teacher');
        //update student
        Route::post('/dashboard/{student}/update', 'App\Http\Controllers\StudentsController@update')->middleware('auth.teacher');

        //createsubjects
        Route::get('/students/createsubjects', 'App\Http\Controllers\SubjectsController@createSubject')->middleware('auth.teacher');
        //storesubjects
        Route::post('/students/subjectstore', 'App\Http\Controllers\SubjectsController@storeSubject')->middleware('auth.teacher');
        //edit subject
        Route::get('/dashboard/{subjects}/editsubject', 'App\Http\Controllers\SubjectsController@editSubject')->middleware('auth.teacher');
        //update subject
        Route::post('/dashboard/{subjects}/updatesubject', 'App\Http\Controllers\SubjectsController@updateSubject')->middleware('auth.teacher');

        //showAll
        Route::get('/dashboard/showAll', 'App\Http\Controllers\StudentsController@showAll')->middleware('auth.teacher');
        //add date of absences to student by subject
        Route::get('/dashboard/{student}/addabsences', 'App\Http\Controllers\StudentsController@addAbsentDate')->middleware('auth.teacher');
        //store date of absences to student by subject
        Route::post('/dashboard/storeabsences', 'App\Http\Controllers\StudentsController@storeAbsentDate')->middleware('auth.teacher');
        //add number of absences to student by subject
        Route::get('/dashboard/{student}/addnumberabsences', 'App\Http\Controllers\StudentsController@addAbsentNumber')->middleware('auth.teacher');
        //update number of absences to student by subject
        Route::post('/dashboard/{number_absents}/updatenumberabsences', 'App\Http\Controllers\StudentsController@updateAbsentNumber')->middleware('auth.teacher');

        //add date of tardiness
        Route::get('/dashboard/{student}/addtardiness', 'App\Http\Controllers\StudentsController@addTardiness')->middleware('auth.teacher');
        //store date of tardiness
        Route::post('/dashboard/storetardiness', 'App\Http\Controllers\StudentsController@storeTardiness')->middleware('auth.teacher');
         //add number of tardiness
        Route::get('/dashboard/{student}/addnumbertardiness', 'App\Http\Controllers\StudentsController@addTardinessNumber')->middleware('auth.teacher');
        //update number of tardiness
        Route::post('/dashboard/{lates}/updatenumbertardiness', 'App\Http\Controllers\StudentsController@updateTardinessNumber')->middleware('auth.teacher');
        
        //delete one student
        Route::get('/dashboard/{student}/delete', 'App\Http\Controllers\StudentsController@delete')->middleware('auth.teacher');
        //delete one subject
        Route::get('/dashboard/{subject}/delesubject', 'App\Http\Controllers\SubjectsController@deleteSubject')->middleware('auth.teacher');

        //change password
        Route::get('/changepassword','App\Http\Controllers\LoginController@showChangePasswordFormUsers')->middleware('auth.teacher');
        Route::post('/changePw','App\Http\Controllers\LoginController@changePasswordUsers')->name('changePw')->middleware('auth.teacher');

        //search
        Route::get('/search/','App\Http\Controllers\StudentsController@search')->middleware('auth.teacher');
        Route::get('/searchmain','App\Http\Controllers\StudentsController@searchMain')->middleware('auth.teacher');



});