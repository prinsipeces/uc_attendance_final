<?php

namespace App\Http\Controllers;

use App\Models\Absent;
use App\Models\AcademicProgram;
use App\Models\Adviser;
use App\Models\Late;
use App\Models\Student;
use App\Models\Subject;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class StudentsController extends Controller
{   
    //index
     public function index()
    {
       $students = Student::whereIn('user_id', [Auth::user()->id])->orderBy('last_name', 'ASC')->paginate(5);

        return view('students.index', compact('students'));
    }

    //create student
    public function create()
    {
        $academic_programs = AcademicProgram::all();
        $users = User::all();
        $advisers = Adviser::all();

        return view('students.createstudents', compact('academic_programs', 'users', 'advisers'));
    }
    //store student
    public function store()
    {
        // validate the form
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'academic_program_id' => 'required',
            'user_id' => 'required',
            'adviser_id' => 'required'
            
        ]);
        // store the model
        $students = Student::firstOrCreate(request()->only([
            'first_name', 'middle_initial', 'last_name', 'academic_program_id', 'user_id', 'adviser_id'
        ]));
        // redirect to index
        return redirect('/students');
    }

    //showall students
    public function showAll()
    {   
        $students = Student::whereIn('user_id', [Auth::user()->id])->orderBy('last_name', 'ASC')->paginate(5);

        return view('dashboard/showAll', compact('students'));
    }
   
    //edit student
    public function edit(Student $student)
    {   
        $academic_programs = AcademicProgram::all();
        $students = Student::all();
        $advisers = Adviser::all();

        return view('dashboard/edit', compact('student', 'students', 'academic_programs', 'advisers'));
    }
    //update edited student
    public function update(Student $student)
    {
        request()->validate([
            'first_name' => 'required',
            'middle_initial' => 'required',
            'last_name' => 'required',
            'academic_program_id' => 'required',
            'adviser_id' => 'required',
        ]);
        $student->update(request()->only([
            'first_name', 'middle_initial', 'last_name', 'academic_program_id', 'adviser_id'
        ]));

        return redirect('dashboard/showAll');
    }

     //add date of absences to student
    public function addAbsentDate(Student $student)
    {
        return view('dashboard.addabsences', compact('student'));
    }
    //store date of absences to student
    public function storeAbsentDate()
    {
        // validate the form
        request()->validate([
            'date' => 'required',
            'student_id' => 'required',
        ]);
        // store the model
        $absents = Absent::create(request()->only([
            'date', 'student_id'
        ]));
        // redirect to index
        return redirect('dashboard/showAll');
    }
     //add number of absences to student
    public function addAbsentNumber(Student $student)
    {       
        //not yet done; to be updated
        return view('dashboard.addnumberabsences', compact('student'));
    }
    //update number of absences to student
    public function updateAbsentNumber(Absent $number_absents)
    {
        // validate the form
        request()->validate([
            'num_absent' => 'required',
            'student_id' => 'required',
        ]);
        // store the model
        $number_absents->update(request()->only([
            'num_absent', 'student_id'
        ]));
        // redirect to index

        return redirect('dashboard/showAll');
    }

     //add date of tardiness
    public function addTardiness(Student $student)
    {
       
        return view('dashboard.addtardiness', compact('student'));
    }
    //store date of tardiness
    public function storeTardiness()
    {
        // validate the form
        request()->validate([
            'date' => 'required',
            'student_id' => 'required',

        ]);
        // store the model
        $lates = Late::create(request()->only([
            'date', 'student_id'
        ]));
        // redirect to index
        return redirect('dashboard/showAll');
    }

     //add number of tardiness
    public function addTardinessNumber(Student $student)
    {       
        //not yet done; to be updated
        return view('dashboard.addnumbertardiness', compact('student'));
    }
    //update number of tardiness
    public function updateTardinessNumber(Late $lates)
    {
        // validate the form
        request()->validate([
            'number_late' => 'required',
        ]);
        // store the model
        $lates->update(request()->only([
            'number_late'
        ]));
        // redirect to index

        return redirect('dashboard/showAll');
    }
    //delete student
     public function delete(Student $student)
    {
        
        $student->delete();

        return redirect()->back()->with("error","Deleted!");
    }
    //search on editing panel
     public function search(Request $request)
    {
        $students = Student::whereIn('user_id', [Auth::user()->id])->get();
        $search = \Request::get('search'); 
        $students = Student::whereIn('user_id', [Auth::user()->id])
            ->where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->whereIn('user_id', [Auth::user()->id])
            ->orWhereHas('academic_programs', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('degree_name', 'like', '%'.$search.'%');
            })
            ->orWhereHas('subjects', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('subject_code', 'like', '%'.$search.'%');
            })
            ->orWhereHas('subjects', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('subject_name', 'like', '%'.$search.'%');
            })
            ->orderBy('last_name', 'ASC')
            ->paginate(5);

        return view('dashboard.showAll', compact('students'));
    }
    //main search query on index
     public function searchMain(Request $request)
    {
        $students = Student::whereIn('user_id', [Auth::user()->id])->get();
        $search = \Request::get('search'); 
        $students = Student::whereIn('user_id', [Auth::user()->id])
            ->where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->whereIn('user_id', [Auth::user()->id])
            ->orWhereHas('academic_programs', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('degree_name', 'like', '%'.$search.'%');
            })
            ->orWhereHas('subjects', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('subject_code', 'like', '%'.$search.'%');
            })
            ->orWhereHas('subjects', function ($query) use ($search) {
                $query->whereIn('user_id', [Auth::user()->id])->where('subject_name', 'like', '%'.$search.'%');
            })
            ->orderBy('last_name', 'ASC')
            ->paginate(5);


        return view('students.index', compact('students'));
    }
}
