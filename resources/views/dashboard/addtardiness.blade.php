@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Add Tardiness</h5>
        </div>
        <div class="card-body" style="border:2px solid black;">
            
            <form action="/dashboard/storetardiness" method="POST">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
                <div class="row">

                        <div class="col-sm-4 d-flex">
                            <div class="form-group">
                                <label for="Student Name">Student Name:</label>
                                <select name="student_id" id="" class="form-control">
                                    <option value="{{ $student->id }}">{{ $student->fullname }}</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="Date of Tardiness">Date of Tardiness:</label>
                                <input type="date" name="date" class='form-control'>
                            </div>
                        </div>

                </div>

                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>
@endsection