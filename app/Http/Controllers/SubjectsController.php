<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Student;
use App\Models\Subject;
use App\Models\AcademicProgram;
use App\Models\Absent;
use Auth;
use Illuminate\Http\Request;

class SubjectsController extends Controller
{
    //create subject
    public function createSubject()
    {
        $subjects = Subject::all();
        $academic_programs = AcademicProgram::all();

        return view('students.createsubjects', compact('subjects', 'academic_programs'));
    }
    //store subject
    public function storeSubject()
    {
        // validate the form
        request()->validate([
            'subject_code' => 'required',
            'subject_name' => 'required',
            'subject_start_time' => 'required',
            'subject_end_time' => 'required',
            'student_id' => 'required'

        ]);
        // store the model
        $subjects = Subject::create(request()->only([
            'subject_code', 'subject_name', 'subject_start_day', 'subject_end_day', 'subject_start_time', 'subject_end_time', 'student_id', 'absent_date', 'num_absent',
        ]));
        // redirect to index
        return redirect('/students');
    }

    //edit subject
    public function editSubject(Subject $subjects)
    {   

        return view('dashboard/editsubject', compact('subjects'));
    }
    //update edited subject
    public function updateSubject(Subject $subjects)
    {
        request()->validate([
            'subject_code' => 'required',
            'subject_name' => 'required',
            'subject_start_time' => 'required',
            'subject_end_time' => 'required',
        ]);

        $subjects->update(request()->only([
        	'subject_code', 'subject_name', 'subject_start_day', 'subject_end_day', 'subject_start_time', 'subject_end_time'
            
        ]));
    
        return redirect('dashboard/showAll');
    }

    public function deleteSubject(Subject $subject)
    {
        
        $subject->delete();
        return redirect()->back()->with("error","Deleted!");
    }
}
