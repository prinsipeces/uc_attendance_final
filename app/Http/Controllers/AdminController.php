<?php

namespace App\Http\Controllers;

use App\Models\Adviser;
use App\Models\Role;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{   

     public function index()
    {   

        $students = Student::whereHas('absents', function ($query) {
             $query->where('num_absent', '>=', '3');
        })
        ->orderBy('last_name', 'asc')
        ->paginate(7);

        $users = User::all();
        $advisers = Adviser::all();

       
        return view ('admin.index', compact('students', 'users', 'advisers'));
    }

    //show all students
     public function showStudents()
    {   

        $students = Student::orderBy('last_name', 'asc')->paginate(7);

        $users = User::all();
        $advisers = Adviser::all();

       
        return view ('admin.showallstudents', compact('students', 'users', 'advisers'));
    }


    //create a user(admin or teacher)
     public function createUser()
    {
        $users = User::all();
       // $roles = Role::where('id', 2)->get();
        $roles = Role::all();

        return view ('admin.adduser', compact('users', 'roles'));
    }

     public function storeUser(Request $request)
    {   
         // validate the form
        request()->validate([
            'id_role' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required'       
        ]);
        //store the model
        User::create([
            "id_role" => $request->id_role,
            "username" => $request->username,
            "password" => Hash::make($request->password),
            "first_name" => $request->first_name,
            "middle_initial" => $request->middle_initial,
            "last_name" => $request->last_name
        ])->roles()->sync([$request->input('id_role')]);
        
        return redirect('/admin/showuser');
    }


    //show users
    public function showUser()
    {
        $users = User::paginate(15);

        return view('admin/showuser', compact('users'));
    }

    //edit user
     public function editUser(User $users)
    {

        $roles = Role::all();

        return view('admin/edituser', compact('users', 'roles'));
    }
    //update user
     public function updateUser(Request $request, User $users)
    {
        request()->validate([
            'id_role' => 'required',
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
            
        ]);

        $users->update(array_merge($request->only('id_role', 'username', 'first_name', 'middle_initial', 'last_name'), [
            'password' => Hash::make($request->password)

        ]));

      $users->roles()->sync([$request->input('id_role')]);

        return redirect('admin/showuser');
    }
    //delete user
     public function deleteUser(User $users)
    {
        $users->delete();
        return redirect()->back()->with("error","Deleted!");
    }

    //add adviser
     public function addAdviser()
    {
       
        return view('admin.addadviser');
    }
    //store adviser
    public function storeAdviser()
    {
        // validate the form
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',


        ]);
        // store the model
        $advisers = Adviser::create(request()->only([
            'first_name', 'middle_initial', 'last_name'
        ]));
        // redirect to index
        return redirect('/admin/showadviser');
    }
    //show advisers
    public function showAdviser()
    {
        $advisers = Adviser::orderBy('id', 'ASC')->paginate(15);
        $users = User::all();

        return view('admin/showadviser', compact('advisers', 'users'));
    }

    //edit adviser
     public function editAdviser(Adviser $adviser)
    {

        $advisers = Adviser::all();

        return view('admin/editadviser', compact('adviser', 'advisers'));
    }
    //update adviser
     public function updateAdviser(Request $request, Adviser $adviser)
    {
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        $adviser->update(request()->only([
            'first_name', 'middle_initial', 'last_name'
        ]));


        return redirect('admin/showadviser');
    }
    //delete adviser
     public function deleteAdviser(Adviser $adviser)
    {
        $adviser->delete();

        return redirect()->back()->with("error","Deleted!");
    }

     //search on index/home
    public function searchIndex(Request $request)
    {
        
        $search = \Request::get('search'); 


        $students = Student::whereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->where('first_name','like','%'.$search.'%')
            ->OrwhereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->where('last_name','like','%'.$search.'%')
            ->OrwhereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->whereHas('academic_programs', function ($query) use ($search) {
                $query->where('degree_name', 'like', '%'.$search.'%');
            })
            ->OrwhereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->whereHas('subjects', function ($query) use ($search) {
                $query->where('subject_code', 'like', '%'.$search.'%')
                ->orWhere('subject_name', 'like', '%'.$search.'%');
            })
            ->OrwhereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->whereHas('users', function ($query) use ($search) {
                $query->where('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%');
            })
            ->OrwhereHas('absents', function ($query){
                $query->where('num_absent', '>=', '3');
            })
            ->whereHas('advisers', function ($query) use ($search) {
                $query->where('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%');
            })
            ->orderBy('last_name', 'ASC')
            ->paginate(5);

        return view('admin.index', compact('students'));
    }


    //search on show all students
    public function searchStudent(Request $request)
    {
        $students = Student::all();
        $search = \Request::get('search'); 
        $students = Student::where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orWhereHas('academic_programs', function ($query) use ($search) {
                $query->where('degree_name', 'like', '%'.$search.'%');
            })
            ->orWhereHas('subjects', function ($query) use ($search) {
                $query->where('subject_code', 'like', '%'.$search.'%')
                ->orWhere('subject_name', 'like', '%'.$search.'%');
            })
            ->orWhereHas('users', function ($query) use ($search) {
                $query->where('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%');
            })
            ->orWhereHas('advisers', function ($query) use ($search) {
                $query->where('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%');
            })
            ->orderBy('last_name', 'ASC')
            ->paginate(5);

        return view('admin.showallstudents', compact('students'));
    }


    //search on showusers
    public function searchUser(Request $request)
    {
        $users = User::all();
        $search = \Request::get('search'); 
        $users = User::where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orWhere('username','like','%'.$search.'%')
            ->orWhereHas('roles', function ($query) use ($search) {
                $query->where('role_name', 'like', '%'.$search.'%');
            })
            ->orderBy('id', 'ASC')
            ->paginate(10);

        return view('admin/showuser', compact('users'));
    }

    //search on advisers
    public function searchAdviser(Request $request)
    {
        $advisers = Adviser::all();
        $search = \Request::get('search'); 
        $advisers = Adviser::where('first_name','like','%'.$search.'%')
            ->orWhere('last_name','like','%'.$search.'%')
            ->orderBy('id', 'ASC')
            ->paginate(15);

        return view('admin/showadviser', compact('advisers'));
    }
}
