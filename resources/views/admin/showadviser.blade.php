@extends('layouts.adminlayout')
@section('content')
	<div class="card table-responsive">
		<div class="card-header">
			<h5>Show Advisers</h5>
		</div>
		<div class="card-body">
			<div class="panel-body">
			    @if (session('error'))
			        <div class="alert alert-danger">
			            {{ session('error') }}
			        </div>
			    @endif
			        @if (session('success'))
			            <div class="alert alert-success">
			                {{ session('success') }}
			            </div>
			        @endif
				
					<div class="col-sm-12 d-flex justify-content-end">
						<form action="/advisersearch" method="get">
							<div class="form-group d-flex justify-content-end">
								<input type="text" name="search" class="form-control" placeholder="Search">
								<span class="form-group-btn col-sm-3 d-flex justify-content-end">
									<button type="submit" class="btn btn-primary">Search</button>
								</span>
								<span class="form-group-btn sm-2 d-flex justify-content-end">
									<a href="/admin/showadviser" class="btn btn-dark">Reset</a>
								</span>
							</div>
						</form>
					</div>

					<table style="text-align: center; font-size: 15px; height: 150px;" class="table-bordered table-dark table-hover container">
						<thead>
							<tr style="font-size: 18px">
								<th>ID</th>
								<th>Name</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody >
							@foreach($advisers as $adviser)
								<tr>
									<td>{{ $adviser->id }}</td>
									<td>
										<option>{{ $adviser->advisername }}</option>
									</td>
									<td>{{ $adviser->created_at }}</td>
									<td>{{ $adviser->updated_at }}</td>
									<td>
										<a style="height: 30px;" href="/admin/{{ $adviser->id }}/editadviser" class="btn btn-info">Edit</a>&nbsp
										<a style="height: 30px;" href="/admin/{{ $adviser->id }}/deleteadviser" class="btn btn-danger">Delete</a>
									</td>
									
								</tr>
							@endforeach
						</tbody>
					</table>
					{{ $advisers->links() }}
				</div>
			</div>
	</div>
@endsection
