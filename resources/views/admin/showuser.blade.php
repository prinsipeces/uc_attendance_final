@extends('layouts.adminlayout')
@section('content')
	<div class="card table-responsive">
		<div class="card-header">
			<h5>Show Users</h5>
		</div>
		<div class="card-body">
			<div class="panel-body">
			    @if (session('error'))
			        <div class="alert alert-danger">
			            {{ session('error') }}
			        </div>
			    @endif
			        @if (session('success'))
			            <div class="alert alert-success">
			                {{ session('success') }}
			            </div>
			        @endif
				
					<div class="col-sm-12 d-flex justify-content-end">
						<form action="/usersearch" method="get">
							<div class="form-group d-flex justify-content-end">
								<input type="text" name="search" class="form-control" placeholder="Search">
								<span class="form-group-btn col-sm-3 d-flex justify-content-end">
									<button type="submit" class="btn btn-primary">Search</button>
								</span>
								<span class="form-group-btn sm-2 d-flex justify-content-end">
									<a href="admin/showuser" class="btn btn-dark">Reset</a>
								</span>
							</div>
						</form>
					</div>

					<table style="text-align: center; font-size: 15px; height: 150px;" class="table-bordered table-dark table-hover container">
						<thead>
							<tr style="font-size: 18px">
								<th>Role</th>
								<th>Full Name</th>
								<th>Username</th>
								<th>Created</th>
								<th>Updated</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							@foreach($users as $user)
								<tr>
									<td>@foreach($user->roles as $role)
										<option value="{{ $user->id_role }}">{{ $role->role_name }}</option>
										@endforeach
									</td>
									<td>{{ $user->name }}</td>
									<td> {{ $user->username }} </td>
									<td> {{ $user->created_at}} </td>
									<td> {{ $user->updated_at}} </td>
									<td style="">
										<a href="/admin/{{ $user->id }}/edituser" class="btn btn-info" style="height: 30px;">Edit</a>&nbsp
										<a href="/admin/{{ $user->id }}/deleteuser" class="btn btn-danger" style="height: 30px;">Delete</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{{ $users->links() }}
				</div>
			</div>
	</div>
@endsection
