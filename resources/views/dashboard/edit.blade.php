@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Edit Student</h5>
        </div>
        <div class="card-body">
            <form action="/dashboard/{{ $student->id }}/update" method="POST">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="First Name">First Name:</label>
                            <input type="text" name="first_name" value="{{ $student->first_name }}" class='form-control' required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="Middle Initial">Middle Initial:</label>
                            <input type="text" name="middle_initial" value="{{ $student->middle_initial}}" class="form-control" required>                           
                        </div>                      
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="Last Name">Last Name:</label>
                            <input type="text" name="last_name" value="{{ $student->last_name }}" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="Academic Program">Academic Program</label>
                            <select style="width:175%;" name="academic_program_id" id="" class="form-control" required>
                                    <option></option>
                                @foreach($academic_programs as $category)
                                    <option value="{{ $category->id }}">{{ $category->degree_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-12 d-flex">
                        <div class="form-group">
                            <label for="Adviser">Advised By:</label>
                            <select style="width:175%;" name="adviser_id" id="" class="form-control" required>
                                <option></option>
                                @foreach($advisers as $adviser)
                                    <option value="{{ $adviser->id }}" class="form-control">{{ $adviser->advisername  }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>                    

                </div>
                <br><br><br><br><br><br>
                <br><br><br><br><br><br>
                <br><br><br><br><br><br>

                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>

    

@endsection
