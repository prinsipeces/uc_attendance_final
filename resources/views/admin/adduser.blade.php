@extends('layouts.adminlayout')
@section('content')
	<div class="card">
		<div class="card-header" style="border-top:3px solid black;">
			<h5>Add User</h5>
		</div>
		<div class="card-body" style="border-top:3px solid black;">
			<form action="/admin/storeuser" method="post">
				@csrf
				@if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

				<div class="row">
					<div class="col-sm-12 d-flex">
                        <div class="form-group">
                            <label for="Role">Role:</label>
                            <select name="id_role" id="" class="form-control" required="">
                            	<option></option>
                                	@foreach($roles as $role)
                                		<option value="{{ $role->id }}">{{ $role->role_name }}</option>
                                	@endforeach	
                            </select>
                        </div>
                    </div>
					<div class="col-sm-2 d-flex">
						<div class="form-group">
							<label for="Username">Username:</label>
							<input type="text" name="username" placeholder="Username" class='form-control' required>
						</div>
					</div>

					<div class="col-sm-10 d-flex">
						<div class="form-group">
							<label for="Password">Password:</label>
							<input type="password" name="password" id="password" placeholder="Password" class='form-control' required><i style="margin-left: 90%;cursor: pointer;transform: translate(0,-180%);" class="far fa-eye" id="togglePassword"></i>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="form-group">
							<label for="First Name">First Name:</label>
							<input type="text" name="first_name" placeholder="First Name" class='form-control' required>
						</div>
					</div>
					<div class="col-sm-2 d-flex">
						<div class="form-group">
							<label for="Middle Initial">Middle Initial:</label>
							<input type="text" name="middle_initial" placeholder="Middle Initial" class='form-control'>
						</div>
					</div>
					<div class="col-sm-3 d-flex">
						<div class="form-group">
							<label for="Last Name">Last Name:</label>
							<input type="text" name="last_name" placeholder="Last Name" class='form-control' required>
						</div>
					</div>
					
				</div><br><br>
				<div class="d-flex">
					<div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
					<div class="col-sm-6 d-flex justify-content-end">
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</form>			
		</div>
	</div>

<script>
    const togglePassword = document.querySelector('#togglePassword');
    const password = document.querySelector('#password');

    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });
</script>	
	
@endsection
