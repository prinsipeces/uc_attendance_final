@extends('layouts.adminlayout')
@section('content')
{{-- <style type="text/css" media="print">
 @page {size: landscape;}
</style> --}}

		<div class="res table-responsive">
		<ul>
			<li style="float: left;"><img src="/img/uc_logo.png"></li><br>
			
			<li class="tophead">
				<h6>OFFICE of Student Affairs and Services</h6>
				<h6>STUDENT DEVELOPMENT OFFICE</h6>
				<h6>REPORT ON ABSENCES</h6>
			</li>
		</ul>
		<div class="header">
			<div class="header1">

						<p>College:
							<span style="text-decoration: underline;">College of Information Technologies and Computer Science</span>
						</p>
						
	{{-- search  --}}  	<form action="/searchstudents" method="get" class="form-inline mt-2 mt-md-0 justify-content-end">
                            <div id="printPageButton" class="form-group d-flex justify-content-end">
                                <input id="printPageButton" style="font-size: 12px;" type="text" name="search" class="form-control mr-sm-2" placeholder="Search" aria-label="Search">
                                    <button id="printPageButton" style="font-size: 10px" class="btn btn-primary" type="submit">Search</button>
                                    <a id="printPageButton" style="font-size: 10px" href="/admin/showallstudents" class="btn btn-dark">Reset</a>
                            </div>
                        </form>

	  		<table class="t2">

					<tr>
						<th>Instructor</th>
						<th>Name of Student<br><span style="font-size:7px;">(Last Name, First Name, Middle Initial)</span></th>
						<th>Academic Program</th>
						<th>Subject</th>
						<th>Schedule</th>
						<th>Date of Absences</th>
						<th>Number of Absences</th>
						<th>Date of Tardiness</th>
						<th>Number of Tardiness</th>					
						<th>Adviser</th>
						<th>Date Submitted</th>
					</tr>
				
				<tbody class="tablebody2">
					@foreach($students as $student)

						<tr>
							<td>
								{{ $student->users['name'] }}
							</td>
							<td><option>{{ $student->fullname }}</option></td>
							<td>{{ $student->academic_programs['degree_name'] }}</td>
							<td>
								@foreach($student->subjects as $subject)
									<option>{{ $subject->subname }}</option>
								@endforeach

							</td>
							<td>
								@foreach($student->subjects as $subject)
									<option>{{ $subject->subjectschedule}}</option>
								@endforeach
							</td>

							<td>
								@foreach($student->absents as $absent)
									<option>{{ $absent->date}}</option>
								@endforeach
							</td>

							<td>@foreach($student->absents as $absent)
									{{ $absent->num_absent}}
								@endforeach
							</td>

							<td>
								@foreach($student->lates as $late)
									<option>{{ $late->date}}</option>
								@endforeach
							</td>

							<td>
								@foreach($student->lates as $late)
									{{ $late->number_late}}
								@endforeach
							</td>

							<td>
								{{ $student->advisers['advisername']  ?? '' }}
							</td>


							<td>
								<option>{{ $student->updated_at }}</option>
							</td>

						</tr>
					@endforeach
				</tbody>
			</table>

			<span id="printPageButton">{{ $students->links() }}</span>
			</div>

			<div class="osas">
				<p class="p1">For OSAS Personnel</p>
				<p class="p2">Received by: _____________________________________
					<span>Date Received: </span>
				</p>
			</div>

			<div class="rap">
				<h6>Reminders on Attendance Policy:</h6>
				<p>The instructor will submit to the Discipline Officer of the Office of Student Affairs and Services (OSAS) the names of students who incurred one half of the allowed number of absences which are accumulated unexcused absences, or who incurred (3) consecutive unexcused absences (whichever case comes first). The student will not be admitted in the class until he/she can present an admission/consultation slip issued by the OSAS Discipline Offices. For approved and excused absences, there is no need to report, but faculty still monitors the number of absences of the student.</p>
			</div>

			<div class="last">
				<p class="p1">Control No:_______________</p>
				<p class="p2">UC-VPAA-OSAS-SDO-Form-01 SDS</p>
				<p class="p3">April 03,2014 Rev. 00</p><br>
			</div>

		</div>
		</div>

	
@endsection