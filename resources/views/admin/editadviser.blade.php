@extends('layouts.adminlayout')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Edit User</h5>
        </div>
        <div class="card-body" style="border:2px solid black;">
          
            <form action="/admin/{{ $adviser->id }}/updateadviser" method="POST">
                @csrf
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    
                    <div class="col-sm-12 d-flex" style="border: 5px ridge black;">
                        <h6 style="font-style: italic; text-decoration: underline;">{{ $adviser->advisername}}</h6>
                    </div>

                        <div class="col-sm-4 d-flex">
                            <div class="form-group">
                                <label for="First Name">First Name:</label>
                                <input type="text" name="first_name" value="{{ $adviser->first_name}}" class='form-control'>
                            </div>
                        </div>

                        <div class="col-sm-4 d-flex">
                            <div class="form-group">
                                <label for="Middle Initial">Middle Initial:</label>
                                <input type="text" name="middle_initial" value="{{ $adviser->middle_initial}}" class='form-control'>
                            </div>
                        </div>

                        <div class="col-sm-4 d-flex">
                            <div class="form-group">
                                <label for="Last Name">Last Name:</label>
                                <input type="text" name="last_name" value="{{ $adviser->last_name}}" class='form-control'>
                            </div>
                        </div>
       

                </div>

                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>

@endsection








