<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //paginate
        Paginator::useBootstrap();

        //
        Blade::if('hasrole', function($expression){

            if(Auth::user()) {
                if(Auth::user()->hasAnyRole($expression)) {
                    return true;
                }
            }
            return false;
        });
        
    }
}
