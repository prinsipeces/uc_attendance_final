@extends('layouts.adminlayout')
@section('content')
	<div class="card">
		<div class="card-header" style="border-top:3px solid black;">
			<h5>Add Adviser</h5>
		</div>
		<div class="card-body" style="border-top:3px solid black;">
			<form action="/admin/storeadviser" method="POST">
				@csrf
				@if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif<br>
				<div class="row">

					<div class="col-sm-4">
						<div class="form-group">
							<label for="First Name">First Name:</label>
							<input type="text" name="first_name" placeholder="First Name" class='form-control' required>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="Middle Initial">Middle Initial:</label>
							<input type="text" name="middle_initial" placeholder="Middle Initial" class='form-control'>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="Last Name">Last Name:</label>
							<input type="text" name="last_name" placeholder="Last Name" class='form-control' required>
						</div>
					</div>
					
				</div><br><br><br><br>
				<div class="d-flex">
					<div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
					<div class="col-sm-6 d-flex justify-content-end">
						<button class="btn btn-primary">Save Changes</button>
					</div>
				</div>
			</form>			
		</div>
	</div>
@endsection