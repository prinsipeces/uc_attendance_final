<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
	protected $guarded = [];
    
	public function academic_programs()
    {
    	return $this->belongsTo(AcademicProgram::class, 'academic_program_id');
    }

	public function subjects()
	{
		return $this->hasMany(Subject::class);
	}

    public function absents()
    {
        return $this->hasMany(Absent::class);
    }

    public function lates()
    {
        return $this->hasMany(Late::class);
    }

    public function advisers()
    {
        return $this->belongsTo(Adviser::class, 'adviser_id');
    }

	public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

	 public function getFullNameAttribute()
    {
        return $this->last_name.', '.$this->first_name.' '.$this->middle_initial;
    }

    use HasFactory;
}
