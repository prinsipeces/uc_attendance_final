<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
        <meta name="generator" content="Jekyll v3.8.6">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <title>University of the Cordilleras</title>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
        <link rel="logo icon" type="image/png" href="/img/logo_main.png">
        
        <!-- Favicons -->
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
              <a style="font-size:12px" class="navbar-brand" href="#">REPORT OF ABSENCES</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/students">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a href = '/students/createstudents' class="nav-link">Add Students</a>
                        </li>
                        <li class="nav-item">
                            <a href = '/students/createsubjects' class="nav-link">Add Subjects</a>
                        </li>
                    </ul>   
     
{{-- print --}}<button id="printPageButton" class="btn btn-outline-success my-2 my-sm-0" onclick="window.print()">Print</button>

                    <ul style="margin: 0!important;">
                        <li style="display:block;" class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" style="color:white; margin-right: 20px;" 
                                href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
                                {{ auth()->user()->name }}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="dropdown-divider"></div>
                                <a style="font-size: 13px" class="dropdown-item" href="/dashboard/showAll">Show All</a>
                                <div class="dropdown-divider"></div>
                                <a style="font-size: 13px" class="dropdown-item" href="/changepassword">Change Password</a>
                                <div class="dropdown-divider"></div>
                                <a style="font-size: 13px" class="dropdown-item" href="/logout">Logout</a>
                            </div>
                      </li>  
                    </ul>
                </div>
            </nav>

        </header>
        <main role="main">
            <div class="container" style="margin-top: 100px">
                @yield('content')            
            </div>
        </main>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
    </body>
</html>
