<?php

namespace Database\Seeders;

use App\Models\AcademicProgram;
use Illuminate\Database\Seeder;

class AcademicProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //insert academic program for CITCS
        $programs = [
        	['degree_name' => 'Bachelor of Science in Information Technology- Network Security'],
        	['degree_name' => 'Bachelor of Science in Information Technology- Web Technology'],
        	['degree_name' => 'Bachelor of Science in Computer Science- Digital Arts and Animation'],
        	['degree_name' => 'Bachelor of Science in Computer Science- Mobile Technology'],
        	['degree_name' => 'Bachelor of Science in Computer Science- Embedded Application'],
        	['degree_name' => 'Bachelor of Science in Data Analytics'],
        	['degree_name' => 'Associate in Computer Technology'],

        
        ];
        AcademicProgram::insert($programs);
    }
}
