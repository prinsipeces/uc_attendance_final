@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Edit Subject</h5>
        </div>
        <div class="card-body" style="border:2px solid black;">
            
            <form action="/dashboard/{{ $subjects->id }}/updatesubject" method="POST">
                @csrf
                <div class="row">
                    <div class="col-sm-12 d-flex" style="border: 5px ridge black;">
                        <h6 style="font-style: italic; text-decoration: underline;">{{ $subjects->subname}}</h6>
                    </div>
                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Code">Subject Code:</label>
                                <input type="text" name="subject_code" value="{{ $subjects->subject_code}}" class='form-control'>
                            </div>
                        </div>

                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Name">Subject Name:</label>
                                <input type="text" name="subject_name" value="{{ $subjects->subject_name}}" class='form-control'>
                            </div>
                        </div>

                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Schedule">Start Day: <span style="text-decoration: underline;color:red;">{{ $subjects->subject_start_day }}</span></label>
                                <select name="subject_start_day" class="form-control">
                                    <option value="{{ $subjects->subject_start_day }}">{{ $subjects->subject_start_day }}</option>
                                    <option></option>
                                    <option value="M">M- Monday</option>
                                    <option value="T">T- Tuesday</option>
                                    <option value="W">W- Wednesday</option>
                                    <option value="TH">TH- Thursday</option>
                                    <option value="F">F- Friday</option>
                                    <option value="S">S- Saturday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Schedule">End Day: <span style="text-decoration: underline;color:red;">{{ $subjects->subject_end_day }}</span></label>
                                <select name="subject_end_day" class="form-control">
                                    <option value="{{ $subjects->subject_end_day }}">{{ $subjects->subject_end_day }}</option>
                                    <option></option>
                                    <option value="M">M- Monday</option>
                                    <option value="T">T- Tuesday</option>
                                    <option value="W">W- Wednesday</option>
                                    <option value="TH">TH- Thursday</option>
                                    <option value="F">F- Friday</option>
                                    <option value="S">S- Saturday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Schedule">Start Time:</label>
                                <input type="time" name="subject_start_time" value="{{ $subjects->subject_start_time }}" class='form-control'>
                            </div>
                        </div>
                        <div class="col-sm-2 d-flex">
                            <div class="form-group">
                                <label for="Subject Schedule">End Time:</label>
                                <input type="time" name="subject_end_time" value="{{ $subjects->subject_end_time }}"  class='form-control'>
                            </div>
                        </div>

                </div>

                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>
@endsection








