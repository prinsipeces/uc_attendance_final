@extends('layouts.master')
@section('content')
    <div class="card">
        <div class="card-header" style="border-top:3px solid black;">
            <h5>Add Subject</h5>
        </div>
        <div class="card-body"  style="border-top:3px solid black;">
            <form action="/students/subjectstore" method="POST">
                @csrf
                <div class="row">

                    <div class="col-sm-4 d-flex">
                        <div class="form-group">
                            <label for="Student Name">Student Name:</label><br>
                            <select name="student_id" id="subject_options" class="form-control">
                                @foreach(Auth::user()->students as $category)
                                    <option value="{{ $category->id }}">{{ $category->fullname }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4 d-flex">
                        <div class="form-group">
                            <label for="Subject Code">Subject Code:</label>
                            <input type="text" name="subject_code" placeholder="Subject Code" class='form-control' required>
                        </div>
                    </div>

                    <div class="col-sm-4 d-flex">
                        <div class="form-group">
                            <label for="Subject Name">Subject Name:</label>
                            <input type="text" name="subject_name" placeholder="Subject Name" class='form-control' required>
                        </div>
                    </div>

                    <div class="container">
                        <br><h6 style="text-decoration: underline;">Schedule:</h6>
                    </div>  

                    <div class="col-sm-2 d-flex">
                        <div class="form-group">
                            <label for="Subject Schedule">Start Day:</label>
                            <select name="subject_start_day" class="form-control" required>
                                <option></option>
                                <option value="M">M- Monday</option>
                                <option value="T">T- Tuesday</option>
                                <option value="W">W- Wednesday</option>
                                <option value="TH">TH- Thursday</option>
                                <option value="F">F- Friday</option>
                                <option value="S">S- Saturday</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-10 d-flex">
                        <div class="form-group">
                            <label for="Subject Schedule">End Day:</label>
                            <select name="subject_end_day" class="form-control">
                                <option></option>
                                <option value="M">M- Monday</option>
                                <option value="T">T- Tuesday</option>
                                <option value="W">W- Wednesday</option>
                                <option value="TH">TH- Thursday</option>
                                <option value="F">F- Friday</option>
                                <option value="S">S- Saturday</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2 d-flex">
                        <div class="form-group">
                            <label for="Subject Schedule">Start Time:</label>
                            <input type="time" name="subject_start_time" placeholder="Start Time" class='form-control' required>
                        </div>
                    </div>
                    <div class="col-sm-10 d-flex">
                        <div class="form-group">
                            <label for="Subject Schedule">End Time:</label>
                            <input type="time" name="subject_end_time" placeholder="End Time" class='form-control' required>
                        </div>
                    </div>
    
                </div><br><br>
                <div class="d-flex">
                    <div class="col-sm-6 d-flex">
                        <a href="javascript:history.back()" class="btn btn-primary">Go Back</a>
                    </div>
                    <div class="col-sm-6 d-flex justify-content-end">
                        <button class="btn btn-primary">Save Changes</button>
                    </div>
                </div>
            </form>         
        </div>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">

      $("#subject_options").select2({
            placeholder: "Select Name",
            allowClear: true
        });
</script>

@endsection